import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Authentification from './views/authentification/Authentification';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header" >
          <img src={logo} className="App-logo" alt="logo" />
         
        </div>
        <div>
          <Authentification />
        </div>
        
      </div>
    );
  }
}

export default App;
