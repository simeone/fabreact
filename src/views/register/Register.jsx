import React, { Component } from 'react';
import { Button, FormGroup, form, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';
import axios from 'axios';

class Register extends Component{
    constructor(){
        super();
        this.state = {

        };
    }
    handleChange = (e) => {
      this.setState({
        [e.target.name]: e.target.value
      });
    };

      handleSubmit = async() =>{
        if (!this.state.firstname || !this.state.lastname || !this.state.mail || !this.state.password || !this.state.passwordConfirme) {
                alert('error: "Veuillez remplir tous les champs."');
    
                 return;
           }
      const utilisateur = {
        "nom": this.state.firstname,
        "prenom": this.state.lastname,
        "email": this.state.mail,
        "password": this.state.password,
        "avatar": "string"
      };
      try{
       let reponse = await axios({
          method:'POST',
          headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token'

          },
          data: utilisateur,
          url:'travelkit-app/public/index.php/api/utilisateurs'
        });
        
        (reponse)?alert('success :',reponse):alert('erreur d\'enregistrement33');
      }catch(e){
        alert("erreur",e);
      }
      
      }
    render = () =>{
        return(
        <div className="container">
          <div className="row">
              <div className="col-lg-12">
              <h1>Enregistrer</h1>
              </div>
          </div>
          <div className="row">
          <form className="col-12">
                <FormGroup>
                  <FormControl
                    type="text"
                    placeholder="Prénom"
                    value={this.state.firstname}
                    onChange={this.handleChange}
                    name="firstname"
                    />
                  <FormControl.Feedback />
                </FormGroup>
          </form>
            
            <form className="col-12">
                <FormGroup>
                  <FormControl
                    type="text"
                    placeholder="Nom"
                    value={this.state.lastname}
                    onChange={this.handleChange}
                    name="lastname"
                    />
                  <FormControl.Feedback />
                </FormGroup>
            </form>
            <form className="col-12">
                <FormGroup>
                  <FormControl
                    type="text"
                    placeholder="Adresse e-mail"
                    value={this.state.mail}
                    onChange={this.handleChange}
                    name="mail"
                    />
                  <FormControl.Feedback />
                </FormGroup>
              </form>
              <form className="col-12">
                <FormGroup>
                  <FormControl
                    type="password"
                    placeholder="Mot de passe"
                    value={this.state.password}
                    onChange={this.handleChange}
                    name="password"
                    />
                  <FormControl.Feedback />
                </FormGroup>
              </form>
              <form className="col-12">
                <FormGroup>
                  <FormControl
                    type="password"
                    placeholder="Confirmer le mot de passe"
                    value={this.state.passwordConfirme}
                    onChange={this.handleChange}
                    name="passwordConfirme"
                    />
                  <FormControl.Feedback />
                </FormGroup>
              </form>

            <Button onClick={this.handleSubmit}>Enregistrer</Button>
          </div>
            
          
           
          
        </div>
        );
        
    }
}
export default Register;