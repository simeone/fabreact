import React, { Component } from 'react';
import Login from '../login/Login';
import Register from '../register/Register'

class Authentification extends Component{

    state = {
        initState: 0
    }
    handleCreerCompte = (e) =>{
        e.preventDefault();
        this.setState({ initState: 1});
    }
    render = ()=>{
        return(
            <div className="container">
                   {(this.state.initState === 0)?
                    <Login/>
                   :<Register/>}

                  
                   { (this.state.initState === 0) ?
                        
                        <div className="row">
                        <p>vous n'avez pas de compte? <a href="#" onClick={this.handleCreerCompte}>créer un</a></p>
                   </div>
                           : ''
                    } 
                
                
            </div>
        );
    }
}
export default Authentification;